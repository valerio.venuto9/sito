+++
title = "Info Logistiche"
date = "2022-01-25"
author = "vale"
+++


## La celebrazione 

avverrà il giorno **Sabato** 24 Settembre 2022 alle ore **16:00** alla [Chiesa di Maria SS. del Rosario di Pompei](https://goo.gl/maps/2K92J9JyS5pniRPbA) di Belvedere Marittimo

Dopo la celebrazione ci sposteremo (chi con i propri mezzi e chi con la navetta) al [Ristorante Sapori & Saperi](https://goo.gl/maps/24Ecyv9kPMVgAdKE8) dove festeggeremo per tutta la notte, brindando e sacrificando gli agnelli. Preparatevi a gustare dell'ottima cucina (speriamo) in stile fusion Calabro/Piemontese.

Per raggiungerci liberi di fare come preferite, qui vi elencheremo alcune delle possibilità:

## il viaggio

### in macchina
Presupponendo la partenza da Torino, allora vi aspetteranno secondo [via michelin](https://www.viamichelin.it/web/Smart_road?departure=10024%20Torino%2C%20Torino%2C%20Italia&departureTid=city-1579709&arrival=87021%20Belvedere%20Marittimo%2C%20Cosenza%2C%20Italia&arrivalTid=city-1634832&index=0&vehicle=0&type=0&distance=km&currency=EUR&highway=false&toll=false&vignette=false&orc=false&crossing=true&caravan=false&shouldUseTraffic=false&withBreaks=false&break_frequency=7200&coffee_duration=1200&lunch_duration=3600&diner_duration=3600&night_duration=32400&car=hatchback&fuel=petrol&fuelCost=2.0975&allowance=0&corridor=&departureDate=&arrivalDate=&fuelConsumption=6.8:5.6:5.6&shouldUseNewEngine=false) quasi 11h di viaggio. Il sito suggerisce di prendere l'uscita di Lagonegro per poi prendere la SS585 ma una soluzione alternativa, leggermente più lunga ma un pelo meglio per la strada, è quella di proseguire per Rende e poi tornare "indietro" per Belvedere.

Per chi dovesse viaggiare in auto, vi segnaliamo questo messaggio da parte dei proprietari del ristorante:

```css
Per facilitare l'organizzazione, 
possiamo pensare di far parcheggiare le macchine dei vostri ospiti
in un luogo che si trova tra la marina ed il centro storico
e disporre di uno o due pulmini con autista.
```


### in aereo
Partendo da Torino, l'aeroporto di riferimento è quello di Lamezia Terme. Il viaggio in aereo finisce qui. Per arrivare a Belvedere sarà necessario procedere così:
1. Navetta dall'Aeroporto alla stazione ferroviaria
2. [Treno](http://treni24.it/intercity-556) da Lamezia con cambio a Paola
3. [Treno](https://www.e656.net/orario/treno/5338.html) da Paola con fermata a Belvedere Marittimo

Una volta arrivati la chiesa dista 10 minuti a piedi dalla stazione, una piacevole passeggiata in riva al mare in quella che si spera essere una giornata torrida.
Dalla chiesa al ristorante abbiamo delle navette pronte per farvi finalmente rilassare.

### in treno
Da Torino, la soluzione migliore in questo caso è quella del Frecciarossa/Italo diretto con fermata a Paola.

Da Paola prendere il [Treno](https://www.e656.net/orario/treno/5338.html) per Belvedere Marittimo.

Per ulteriori info vedere su nelle istruzioni "in aereo"

## dove dormirò?
Per la notte di sabato abbiamo prenotato degli appartementi all'interno della struttura del ristorante riservati ai familiari. Nel particolare abbiamo a disposposizione:
* Una camera da 3 posti letto
* Quattro camere da 2 posti letto
* Due appartamenti da 4/5 posti letto (più pargoli)

Per quanto riguarda i gli amici ed altri familiari abbiamo a disposizione un'intera struttura da 60 posti letto con camere da 1,2,3 o 4 posti, tutte con bagno.

Infine (ci saranno aggiornamenti) potremmo avere a disposizione anche un salone con camerata per i più temerari.

In tutto contiamo tra gli 85 ed i 105 posti letto.

Vi chiediamo cortesemente di farci sapere se di gruppo (amici o famiglia) avreste intenzione o preferireste soggiornare, in maniera autonoma, da qualche altra parte, in modo da poterci organizzare meglio con i letti a disposizione.


# TL;DR / F.A.Q.
* Quando?
  * Il 24 Settembre 2022

* Dove?
  * A Belvedere Marittimo (CS)
    * [Chiesa](https://goo.gl/maps/2K92J9JyS5pniRPbA)
    * [Ristorante](https://goo.gl/maps/24Ecyv9kPMVgAdKE8)

* Cosa?
  * Ci sposiamo

* Sono invitatə?
  * probabilmente sì, nel caso chiedi pure

* Come faccio a raggiungervi?
  * Aereo - Aeroporto di Lamezia Terme, poi treno (due cambi, da Areroporto a Lamezia Terme Centrale, da Lamezia Terme Centrale a Paola e da Paola a Belvedere Marittimo)
  * Treno - Da Torino c'è il Frecciarossa/ diretto fino a Paola, poi cambio a Belvedere
  * Macchina - vedi tu

* Dove posso dormire?
  * per i familiari: abbiamo posti letto prenotati nella struttura
  * per gli amici: c'è qualcosa anche per voi, vi contatteremo personalmente
  * se avete già delle soluzioni pronte (es. prendo casa in affitto per una settimana, vado in hotel ecc) allora cortesemente ci fareste sapere?
